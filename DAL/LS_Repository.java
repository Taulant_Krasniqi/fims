/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.DAL;

/**
 *
 * @author SnAp
 */
import fims.BLL.LaboratorSystem;
import java.util.List;
import javax.persistence.Query;

public class LS_Repository extends EntMngClass implements LS_Interface  {

    public void create(LaboratorSystem e) throws C_Exception {
        try {
            em.getTransaction().begin();
            em.persist(e);
            em.getTransaction().commit();
        } catch (Exception ex) {
            throw new C_Exception("Msg \n" + ex.getMessage());
        }
    }

    public void edit(LaboratorSystem e) throws C_Exception {
        try {
            em.getTransaction().begin();
            em.merge(e);
            em.getTransaction().commit();
        } catch (Exception ex) {
            throw new C_Exception("Msg \n" + ex.getMessage());
        }
    }

    public void delete(LaboratorSystem e) throws C_Exception {
        try {
            em.getTransaction().begin();
            em.remove(e);
            em.getTransaction().commit();
        } catch (Exception ex) {
            throw new C_Exception("Msg \n" + ex.getMessage());
        }
    }
    public List<LaboratorSystem> findAll() throws C_Exception {
        try {
            return em.createNamedQuery("LaboratorSystem.findAll").getResultList();
        } catch (Exception ex) {
            throw new C_Exception("Msg! \n" + ex.getMessage());
        }
    }

    public LaboratorSystem findByID(Integer ID) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
     
    


}