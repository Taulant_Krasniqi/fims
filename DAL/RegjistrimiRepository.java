/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.DAL;

import fims.BLL.Login;
import fims.BLL.Regjistrimi;
import javax.persistence.Query;
import javax.security.auth.login.LoginException;

/**
 *
 * @author HP
 */
public class RegjistrimiRepository extends EntMngClass implements Regjistrimi_Interface{
    
    public void create(Regjistrimi r) throws LoginException {
        try {
            em.getTransaction().begin();
            em.persist(r);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new LoginException("Msg \n" + e.getMessage());
        }
    }
     public Regjistrimi findByEmail(String u) throws LoginException{
         try{
             Query query = em.createQuery("SELECT e FROM Regjistrimi e WHERE e.email = :email");
             query.setParameter("email", u);
             return (Regjistrimi) query.getSingleResult();
         }catch(Exception e){
             throw new LoginException("Msg \n"+ e.getMessage());
         }
     }
     public Regjistrimi findByNickname(String u) throws LoginException{
         try{
             Query query = em.createQuery("SELECT e FROM Regjistrimi e WHERE e.nickname = :nickname");
             query.setParameter("nickname", u);
             return (Regjistrimi) query.getSingleResult();
         }catch(Exception e){
             throw new LoginException("Msg \n"+ e.getMessage());
         }
     }
    
}
