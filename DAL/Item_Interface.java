/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.DAL;

import fims.BLL.ItemInformation;
import java.util.List;

/**
 *
 * @author HP
 */
public interface Item_Interface {
    
    
    void create(ItemInformation p) throws C_Exception;

    void edit(ItemInformation p) throws C_Exception;

    void delete(ItemInformation p) throws C_Exception;

    List<ItemInformation> findAll() throws C_Exception;
}
