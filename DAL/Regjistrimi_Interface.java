/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.DAL;

import fims.BLL.Regjistrimi;
import javax.security.auth.login.LoginException;

/**
 *
 * @author HP
 */
public interface Regjistrimi_Interface {
    


    void create(Regjistrimi p) throws LoginException;

    Regjistrimi findByEmail(String u) throws LoginException;

    Regjistrimi findByNickname(String u)throws LoginException;

}
