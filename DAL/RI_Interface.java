/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.DAL;

import fims.BLL.ReportersInformation;
import java.util.List;

/**
 *
 * @author HP
 */
public interface RI_Interface {
    
    
    
     void create(ReportersInformation p) throws C_Exception;

    void edit(ReportersInformation p) throws C_Exception;

    void delete(ReportersInformation p) throws C_Exception;

    List<ReportersInformation> findAll() throws C_Exception;
}
