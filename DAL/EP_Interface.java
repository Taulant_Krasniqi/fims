/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.DAL;

import fims.BLL.EvidencePackaging;
import java.util.List;

/**
 *
 * @author HP
 */
public interface EP_Interface {
    
    
     void create(EvidencePackaging p) throws C_Exception;

    void edit(EvidencePackaging p) throws C_Exception;

    void delete(EvidencePackaging p) throws C_Exception;

    List<EvidencePackaging> findAll() throws C_Exception;
}
