/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.DAL;

import fims.BLL.AnalysisTimeInformation;

import java.util.List;

/**
 *
 * @author Liridon
 */
public interface ATI_Interface {

    void create(AnalysisTimeInformation p) throws C_Exception;

    void edit(AnalysisTimeInformation p) throws C_Exception;

    void delete(AnalysisTimeInformation p) throws C_Exception;

    List<AnalysisTimeInformation> findAll() throws C_Exception;

}
