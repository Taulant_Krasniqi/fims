/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.DAL;

/**
 *
 * @author HP
 */
import fims.BLL.Login;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.security.auth.login.LoginException;
public class AdminLoginRepository extends EntMngClass implements AdminLogin_Interface{
    
    
    @Override
    public void create(Login al) throws C_Exception{
        try {
            em.getTransaction().begin();
            em.persist(al);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new C_Exception("Msg \n" + e.getMessage());
        }
    }


    @Override
       public Login findByNameAndPassword(String u, String p)throws LoginException{  
        try{
            Query query = em.createQuery("SELECT e FROM Login e WHERE e.nickName = :emri and e.password = :password");
        
            query.setParameter("emri", u);
        
            query.setParameter("password", p);
        
            return (Login)query.getSingleResult();
        }catch(Exception e){
            throw new LoginException("Msg \n" + e.getMessage());
        }     
     }
    @Override
       public Login findByName(String u)throws LoginException{
           try{
               Query query = em.createQuery("SELECT e FROM Login e WHERE e.NickName = :emri");
               query.setParameter("emri", u);
               return (Login)query.getSingleResult();
           }catch(Exception e){
               throw new LoginException("Msg \n" + e.getMessage());
           }
           
       }
}
