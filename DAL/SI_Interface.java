/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.DAL;

import fims.BLL.SuspectInformation;
import java.util.List;

/**
 *
 * @author HP
 */
public interface SI_Interface {
    
    
    void create(SuspectInformation p) throws C_Exception;

    void edit(SuspectInformation p) throws C_Exception;

    void delete(SuspectInformation p) throws C_Exception;

    List<SuspectInformation> findAll() throws C_Exception;
}
