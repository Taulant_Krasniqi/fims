/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.DAL;

import java.util.List;
import fims.BLL.ExctractionInformation;

/**
 *
 * @author HP
 */
public interface EI_Interface {

    void create(ExctractionInformation p) throws C_Exception;

    void edit(ExctractionInformation p) throws C_Exception;

    void delete(ExctractionInformation p) throws C_Exception;

    List<ExctractionInformation> findAll() throws C_Exception;


}
