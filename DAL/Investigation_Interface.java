/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.DAL;

import fims.BLL.InvestigationInformation;
import java.util.List;

/**
 *
 * @author HP
 */
public interface Investigation_Interface {
    
    
    void create(InvestigationInformation p) throws C_Exception;

    void edit(InvestigationInformation p) throws C_Exception;

    void delete(InvestigationInformation p) throws C_Exception;

    List<InvestigationInformation> findAll() throws C_Exception;
}
