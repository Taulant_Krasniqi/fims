/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.DAL;

import fims.BLL.LaboratorSystem;
import java.util.List;

/**
 *
 * @author HP
 */
public interface LS_Interface {
    
    
    void create(LaboratorSystem p) throws C_Exception;

    void edit(LaboratorSystem p) throws C_Exception;

    void delete(LaboratorSystem p) throws C_Exception;

    List<LaboratorSystem> findAll() throws C_Exception;
}
