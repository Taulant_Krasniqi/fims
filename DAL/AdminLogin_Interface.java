/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.DAL;
import fims.BLL.Login;
import java.util.List;
import javax.security.auth.login.LoginException;
/**
 *
 * @author HP
 */
public interface AdminLogin_Interface {

    void create(Login p) throws C_Exception;

    Login findByNameAndPassword(String u, String p) throws LoginException;

    Login findByName(String u)throws LoginException;

}
