/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.DAL;

import fims.BLL.VictimInformation;
import java.util.List;

/**
 *
 * @author HP
 */
public interface VI_Interface {
    
    
    void create(VictimInformation p) throws C_Exception;

    void edit(VictimInformation p) throws C_Exception;

    void delete(VictimInformation p) throws C_Exception;

    List<VictimInformation> findAll() throws C_Exception;

}
