/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.BLL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "Item_Information")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ItemInformation.findAll", query = "SELECT i FROM ItemInformation i")
    , @NamedQuery(name = "ItemInformation.findByItemID", query = "SELECT i FROM ItemInformation i WHERE i.itemID = :itemID")
    , @NamedQuery(name = "ItemInformation.findByItemsDescription", query = "SELECT i FROM ItemInformation i WHERE i.itemsDescription = :itemsDescription")})
public class ItemInformation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ItemID")
    @GeneratedValue(generator="InvSeq")
    @SequenceGenerator(name="InvSeq",sequenceName="INV_SEQ", allocationSize=1)
    private Integer itemID;
    @Basic(optional = false)
    @Column(name = "Items_Description")
    private String itemsDescription;

    public ItemInformation() {
    }

    public ItemInformation(Integer itemID) {
        this.itemID = itemID;
    }

    public ItemInformation(Integer itemID, String itemsDescription) {
        this.itemID = itemID;
        this.itemsDescription = itemsDescription;
    }

    public Integer getItemID() {
        return itemID;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }

    public String getItemsDescription() {
        return itemsDescription;
    }

    public void setItemsDescription(String itemsDescription) {
        this.itemsDescription = itemsDescription;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (itemID != null ? itemID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemInformation)) {
            return false;
        }
        ItemInformation other = (ItemInformation) object;
        if ((this.itemID == null && other.itemID != null) || (this.itemID != null && !this.itemID.equals(other.itemID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fims.BLL.ItemInformation[ itemID=" + itemID + " ]";
    }
    
}
