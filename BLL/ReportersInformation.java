/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.BLL;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "Reporters_Information")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReportersInformation.findAll", query = "SELECT r FROM ReportersInformation r")
    , @NamedQuery(name = "ReportersInformation.findByReporterID", query = "SELECT r FROM ReportersInformation r WHERE r.reporterID = :reporterID")
    , @NamedQuery(name = "ReportersInformation.findByLabCaseNumber", query = "SELECT r FROM ReportersInformation r WHERE r.labCaseNumber = :labCaseNumber")
    , @NamedQuery(name = "ReportersInformation.findByDateOfReport", query = "SELECT r FROM ReportersInformation r WHERE r.dateOfReport = :dateOfReport")
    , @NamedQuery(name = "ReportersInformation.findByReporterName", query = "SELECT r FROM ReportersInformation r WHERE r.reporterName = :reporterName")
    , @NamedQuery(name = "ReportersInformation.findByReporterAddress", query = "SELECT r FROM ReportersInformation r WHERE r.reporterAddress = :reporterAddress")
    , @NamedQuery(name = "ReportersInformation.findByCity", query = "SELECT r FROM ReportersInformation r WHERE r.city = :city")
    , @NamedQuery(name = "ReportersInformation.findByZipCode", query = "SELECT r FROM ReportersInformation r WHERE r.zipCode = :zipCode")
    , @NamedQuery(name = "ReportersInformation.findByState", query = "SELECT r FROM ReportersInformation r WHERE r.state = :state")
    , @NamedQuery(name = "ReportersInformation.findByPhoneNumber", query = "SELECT r FROM ReportersInformation r WHERE r.phoneNumber = :phoneNumber")})
public class ReportersInformation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ReporterID")
    @GeneratedValue(generator="InvSeq")
    @SequenceGenerator(name="InvSeq",sequenceName="INV_SEQ", allocationSize=1)
    private Integer reporterID;
    @Basic(optional = false)
    @Column(name = "Lab_Case_Number")
    private String labCaseNumber;
    @Basic(optional = false)
    @Column(name = "Date_Of_Report")
    @Temporal(TemporalType.DATE)
    private Date dateOfReport;
    @Basic(optional = false)
    @Column(name = "Reporter_Name")
    private String reporterName;
    @Basic(optional = false)
    @Column(name = "Reporter_Address")
    private String reporterAddress;
    @Basic(optional = false)
    @Column(name = "City")
    private String city;
    @Column(name = "Zip_Code")
    private String zipCode;
    @Basic(optional = false)
    @Column(name = "State")
    private String state;
    @Basic(optional = false)
    @Column(name = "PhoneNumber")
    private String phoneNumber;
    @JoinColumn(name = "LaboratorID", referencedColumnName = "LaboratorID")
    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    private LaboratorSystem laboratorID;

    public ReportersInformation() {
    }

    public ReportersInformation(Integer reporterID) {
        this.reporterID = reporterID;
    }

    public ReportersInformation(Integer reporterID, String labCaseNumber, Date dateOfReport, String reporterName, String reporterAddress, String city, String state, String phoneNumber) {
        this.reporterID = reporterID;
        this.labCaseNumber = labCaseNumber;
        this.dateOfReport = dateOfReport;
        this.reporterName = reporterName;
        this.reporterAddress = reporterAddress;
        this.city = city;
        this.state = state;
        this.phoneNumber = phoneNumber;
    }

    public Integer getReporterID() {
        return reporterID;
    }

    public void setReporterID(Integer reporterID) {
        this.reporterID = reporterID;
    }

    public String getLabCaseNumber() {
        return labCaseNumber;
    }

    public void setLabCaseNumber(String labCaseNumber) {
        this.labCaseNumber = labCaseNumber;
    }

    public Date getDateOfReport() {
        return dateOfReport;
    }

    public void setDateOfReport(Date dateOfReport) {
        this.dateOfReport = dateOfReport;
    }

    public String getReporterName() {
        return reporterName;
    }

    public void setReporterName(String reporterName) {
        this.reporterName = reporterName;
    }

    public String getReporterAddress() {
        return reporterAddress;
    }

    public void setReporterAddress(String reporterAddress) {
        this.reporterAddress = reporterAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public LaboratorSystem getLaboratorID() {
        return laboratorID;
    }

    public void setLaboratorID(LaboratorSystem laboratorID) {
        this.laboratorID = laboratorID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reporterID != null ? reporterID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReportersInformation)) {
            return false;
        }
        ReportersInformation other = (ReportersInformation) object;
        if ((this.reporterID == null && other.reporterID != null) || (this.reporterID != null && !this.reporterID.equals(other.reporterID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fims.BLL.ReportersInformation[ reporterID=" + reporterID + " ]";
    }
    
}
