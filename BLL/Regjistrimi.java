/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.BLL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "Regjistrimi")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Regjistrimi.findAll", query = "SELECT r FROM Regjistrimi r")
    , @NamedQuery(name = "Regjistrimi.findByRegjistrimiID", query = "SELECT r FROM Regjistrimi r WHERE r.regjistrimiID = :regjistrimiID")
    , @NamedQuery(name = "Regjistrimi.findByEmri", query = "SELECT r FROM Regjistrimi r WHERE r.emri = :emri")
    , @NamedQuery(name = "Regjistrimi.findByMbiemri", query = "SELECT r FROM Regjistrimi r WHERE r.mbiemri = :mbiemri")
    , @NamedQuery(name = "Regjistrimi.findByEmail", query = "SELECT r FROM Regjistrimi r WHERE r.email = :email")
    , @NamedQuery(name = "Regjistrimi.findByPassword", query = "SELECT r FROM Regjistrimi r WHERE r.password = :password")
    , @NamedQuery(name = "Regjistrimi.findByNickName", query = "SELECT r FROM Regjistrimi r WHERE r.nickName = :nickName")})
public class Regjistrimi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "RegjistrimiID")
    @GeneratedValue(generator="InvSeq")
    @SequenceGenerator(name="InvSeq",sequenceName="INV_SEQ", allocationSize=1)
    private Integer regjistrimiID;
    @Basic(optional = false)
    @Column(name = "Emri")
    private String emri;
    @Basic(optional = false)
    @Column(name = "Mbiemri")
    private String mbiemri;
    @Basic(optional = false)
    @Column(name = "Email")
    private String email;
    @Basic(optional = false)
    @Column(name = "Password")
    private String password;
    @Basic(optional = false)
    @Column(name = "NickName")
    private String nickName;
    @JoinColumn(name = "LoginID", referencedColumnName = "LoginID")
    @ManyToOne(optional = false)
    private Login loginID;

    public Regjistrimi() {
    }

    public Regjistrimi(Integer regjistrimiID) {
        this.regjistrimiID = regjistrimiID;
    }

    public Regjistrimi(Integer regjistrimiID, String emri, String mbiemri, String email, String password, String nickName) {
        this.regjistrimiID = regjistrimiID;
        this.emri = emri;
        this.mbiemri = mbiemri;
        this.email = email;
        this.password = password;
        this.nickName = nickName;
    }

    public Integer getRegjistrimiID() {
        return regjistrimiID;
    }

    public void setRegjistrimiID(Integer regjistrimiID) {
        this.regjistrimiID = regjistrimiID;
    }

    public String getEmri() {
        return emri;
    }

    public void setEmri(String emri) {
        this.emri = emri;
    }

    public String getMbiemri() {
        return mbiemri;
    }

    public void setMbiemri(String mbiemri) {
        this.mbiemri = mbiemri;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Login getLoginID() {
        return loginID;
    }

    public void setLoginID(Login loginID) {
        this.loginID = loginID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (regjistrimiID != null ? regjistrimiID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Regjistrimi)) {
            return false;
        }
        Regjistrimi other = (Regjistrimi) object;
        if ((this.regjistrimiID == null && other.regjistrimiID != null) || (this.regjistrimiID != null && !this.regjistrimiID.equals(other.regjistrimiID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fims.BLL.Regjistrimi[ regjistrimiID=" + regjistrimiID + " ]";
    }
    
}
