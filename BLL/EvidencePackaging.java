/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.BLL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "Evidence_Packaging")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EvidencePackaging.findAll", query = "SELECT e FROM EvidencePackaging e")
    , @NamedQuery(name = "EvidencePackaging.findByEvidencePackageID", query = "SELECT e FROM EvidencePackaging e WHERE e.evidencePackageID = :evidencePackageID")
    , @NamedQuery(name = "EvidencePackaging.findByLabCaseNumber", query = "SELECT e FROM EvidencePackaging e WHERE e.labCaseNumber = :labCaseNumber")
    , @NamedQuery(name = "EvidencePackaging.findByCommentPackaging", query = "SELECT e FROM EvidencePackaging e WHERE e.commentPackaging = :commentPackaging")
    , @NamedQuery(name = "EvidencePackaging.findByPackageNumber", query = "SELECT e FROM EvidencePackaging e WHERE e.packageNumber = :packageNumber")})
public class EvidencePackaging implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "EvidencePackageID")
    @GeneratedValue(generator="InvSeq")
    @SequenceGenerator(name="InvSeq",sequenceName="INV_SEQ", allocationSize=1)
    private Integer evidencePackageID;
    @Basic(optional = false)
    @Column(name = "Lab_Case_Number")
    private String labCaseNumber;
    @Basic(optional = false)
    @Column(name = "Comment_Packaging")
    private String commentPackaging;
    @Basic(optional = false)
    @Column(name = "Package_Number")
    private int packageNumber;
    @JoinColumn(name = "LaboratorID", referencedColumnName = "LaboratorID")
    @ManyToOne(cascade = CascadeType.ALL,optional = false)
    private LaboratorSystem laboratorID;

    public EvidencePackaging() {
    }

    public EvidencePackaging(Integer evidencePackageID) {
        this.evidencePackageID = evidencePackageID;
    }

    public EvidencePackaging(Integer evidencePackageID, String labCaseNumber, String commentPackaging, int packageNumber) {
        this.evidencePackageID = evidencePackageID;
        this.labCaseNumber = labCaseNumber;
        this.commentPackaging = commentPackaging;
        this.packageNumber = packageNumber;
    }

    public Integer getEvidencePackageID() {
        return evidencePackageID;
    }

    public void setEvidencePackageID(Integer evidencePackageID) {
        this.evidencePackageID = evidencePackageID;
    }

    public String getLabCaseNumber() {
        return labCaseNumber;
    }

    public void setLabCaseNumber(String labCaseNumber) {
        this.labCaseNumber = labCaseNumber;
    }

    public String getCommentPackaging() {
        return commentPackaging;
    }

    public void setCommentPackaging(String commentPackaging) {
        this.commentPackaging = commentPackaging;
    }

    public int getPackageNumber() {
        return packageNumber;
    }

    public void setPackageNumber(int packageNumber) {
        this.packageNumber = packageNumber;
    }

    public LaboratorSystem getLaboratorID() {
        return laboratorID;
    }

    public void setLaboratorID(LaboratorSystem laboratorID) {
        this.laboratorID = laboratorID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (evidencePackageID != null ? evidencePackageID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EvidencePackaging)) {
            return false;
        }
        EvidencePackaging other = (EvidencePackaging) object;
        if ((this.evidencePackageID == null && other.evidencePackageID != null) || (this.evidencePackageID != null && !this.evidencePackageID.equals(other.evidencePackageID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fims.BLL.EvidencePackaging[ evidencePackageID=" + evidencePackageID + " ]";
    }
    
}
