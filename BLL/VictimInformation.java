/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.BLL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "Victim_Information")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VictimInformation.findAll", query = "SELECT v FROM VictimInformation v")
    , @NamedQuery(name = "VictimInformation.findByVictimID", query = "SELECT v FROM VictimInformation v WHERE v.victimID = :victimID")
    , @NamedQuery(name = "VictimInformation.findByFullName", query = "SELECT v FROM VictimInformation v WHERE v.fullName = :fullName")
    , @NamedQuery(name = "VictimInformation.findByGender", query = "SELECT v FROM VictimInformation v WHERE v.gender = :gender")
    , @NamedQuery(name = "VictimInformation.findByDateOfBirth", query = "SELECT v FROM VictimInformation v WHERE v.dateOfBirth = :dateOfBirth")
    , @NamedQuery(name = "VictimInformation.findByAddress", query = "SELECT v FROM VictimInformation v WHERE v.address = :address")
    , @NamedQuery(name = "VictimInformation.findByCity", query = "SELECT v FROM VictimInformation v WHERE v.city = :city")
    , @NamedQuery(name = "VictimInformation.findByState", query = "SELECT v FROM VictimInformation v WHERE v.state = :state")
    , @NamedQuery(name = "VictimInformation.findByZipCode", query = "SELECT v FROM VictimInformation v WHERE v.zipCode = :zipCode")})
public class VictimInformation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "VictimID")
    @GeneratedValue(generator="InvSeq")
    @SequenceGenerator(name="InvSeq",sequenceName="INV_SEQ", allocationSize=1)
    private Integer victimID;
    @Basic(optional = false)
    @Column(name = "Full_Name")
    private String fullName;
    @Basic(optional = false)
    @Column(name = "Gender")
    private String gender;
    @Basic(optional = false)
    @Column(name = "Date_Of_Birth")
    private String dateOfBirth;
    @Basic(optional = false)
    @Column(name = "Address")
    private String address;
    @Basic(optional = false)
    @Column(name = "City")
    private String city;
    @Basic(optional = false)
    @Column(name = "State")
    private String state;
    @Basic(optional = false)
    @Column(name = "Zip_Code")
    private String zipCode;

    public VictimInformation() {
    }

    public VictimInformation(Integer victimID) {
        this.victimID = victimID;
    }

    public VictimInformation(Integer victimID, String fullName, String gender, String dateOfBirth, String address, String city, String state, String zipCode) {
        this.victimID = victimID;
        this.fullName = fullName;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
    }

    public Integer getVictimID() {
        return victimID;
    }

    public void setVictimID(Integer victimID) {
        this.victimID = victimID;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (victimID != null ? victimID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VictimInformation)) {
            return false;
        }
        VictimInformation other = (VictimInformation) object;
        if ((this.victimID == null && other.victimID != null) || (this.victimID != null && !this.victimID.equals(other.victimID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fims.BLL.VictimInformation[ victimID=" + victimID + " ]";
    }
    
}
