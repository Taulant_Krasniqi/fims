/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "Laborator_System")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LaboratorSystem.findAll", query = "SELECT l FROM LaboratorSystem l")
    , @NamedQuery(name = "LaboratorSystem.findByLaboratorID", query = "SELECT l FROM LaboratorSystem l WHERE l.laboratorID = :laboratorID")
    , @NamedQuery(name = "LaboratorSystem.findByGenerateUniqueNumber", query = "SELECT l FROM LaboratorSystem l WHERE l.generateUniqueNumber = :generateUniqueNumber")})
public class LaboratorSystem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "LaboratorID")
    @GeneratedValue(generator="InvSeq")
    @SequenceGenerator(name="InvSeq",sequenceName="INV_SEQ", allocationSize=1)
    private Integer laboratorID;
    @Basic(optional = false)
    @Column(name = "Generate_Unique_Number")
    private long generateUniqueNumber;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "laboratorID")
    private Collection<ReportersInformation> reportersInformationCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "laboratorID")
    private Collection<EvidencePackaging> evidencePackagingCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "laboratorID")
    private Collection<InvestigationInformation> investigationInformationCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "laboratorID")
    private Collection<ExctractionInformation> exctractionInformationCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "laboratorID")
    private Collection<AnalysisTimeInformation> analysisTimeInformationCollection;

    public LaboratorSystem() {
    }

    public LaboratorSystem(Integer laboratorID) {
        this.laboratorID = laboratorID;
    }

    public LaboratorSystem(Integer laboratorID, long generateUniqueNumber) {
        this.laboratorID = laboratorID;
        this.generateUniqueNumber = generateUniqueNumber;
    }

    public Integer getLaboratorID() {
        return laboratorID;
    }

    public void setLaboratorID(Integer laboratorID) {
        this.laboratorID = laboratorID;
    }

    public long getGenerateUniqueNumber() {
        return generateUniqueNumber;
    }

    public void setGenerateUniqueNumber(long generateUniqueNumber) {
        this.generateUniqueNumber = generateUniqueNumber;
    }

    @XmlTransient
    public Collection<ReportersInformation> getReportersInformationCollection() {
        return reportersInformationCollection;
    }

    public void setReportersInformationCollection(Collection<ReportersInformation> reportersInformationCollection) {
        this.reportersInformationCollection = reportersInformationCollection;
    }

    @XmlTransient
    public Collection<EvidencePackaging> getEvidencePackagingCollection() {
        return evidencePackagingCollection;
    }

    public void setEvidencePackagingCollection(Collection<EvidencePackaging> evidencePackagingCollection) {
        this.evidencePackagingCollection = evidencePackagingCollection;
    }

    @XmlTransient
    public Collection<InvestigationInformation> getInvestigationInformationCollection() {
        return investigationInformationCollection;
    }

    public void setInvestigationInformationCollection(Collection<InvestigationInformation> investigationInformationCollection) {
        this.investigationInformationCollection = investigationInformationCollection;
    }

    @XmlTransient
    public Collection<ExctractionInformation> getExctractionInformationCollection() {
        return exctractionInformationCollection;
    }

    public void setExctractionInformationCollection(Collection<ExctractionInformation> exctractionInformationCollection) {
        this.exctractionInformationCollection = exctractionInformationCollection;
    }

    @XmlTransient
    public Collection<AnalysisTimeInformation> getAnalysisTimeInformationCollection() {
        return analysisTimeInformationCollection;
    }

    public void setAnalysisTimeInformationCollection(Collection<AnalysisTimeInformation> analysisTimeInformationCollection) {
        this.analysisTimeInformationCollection = analysisTimeInformationCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (laboratorID != null ? laboratorID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LaboratorSystem)) {
            return false;
        }
        LaboratorSystem other = (LaboratorSystem) object;
        if ((this.laboratorID == null && other.laboratorID != null) || (this.laboratorID != null && !this.laboratorID.equals(other.laboratorID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fims.BLL.LaboratorSystem[ laboratorID=" + laboratorID + " ]";
    }
    
}
