/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.BLL;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "Exctraction_Information")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExctractionInformation.findAll", query = "SELECT e FROM ExctractionInformation e")
    , @NamedQuery(name = "ExctractionInformation.findByPreparationID", query = "SELECT e FROM ExctractionInformation e WHERE e.preparationID = :preparationID")
    , @NamedQuery(name = "ExctractionInformation.findByLabCaseNumber", query = "SELECT e FROM ExctractionInformation e WHERE e.labCaseNumber = :labCaseNumber")
    , @NamedQuery(name = "ExctractionInformation.findByExtractionMethod", query = "SELECT e FROM ExctractionInformation e WHERE e.extractionMethod = :extractionMethod")
    , @NamedQuery(name = "ExctractionInformation.findByDate", query = "SELECT e FROM ExctractionInformation e WHERE e.date = :date")
    , @NamedQuery(name = "ExctractionInformation.findByExctractionInfo", query = "SELECT e FROM ExctractionInformation e WHERE e.exctractionInfo = :exctractionInfo")})
public class ExctractionInformation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PreparationID")
    @GeneratedValue(generator="InvSeq")
    @SequenceGenerator(name="InvSeq",sequenceName="INV_SEQ", allocationSize=1)
    private Integer preparationID;
    @Basic(optional = false)
    @Column(name = "Lab_Case_Number")
    private String labCaseNumber;
    @Basic(optional = false)
    @Column(name = "Extraction_Method")
    private String extractionMethod;
    @Basic(optional = false)
    @Column(name = "Date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Basic(optional = false)
    @Column(name = "Exctraction_Info")
    private String exctractionInfo;
    @JoinColumn(name = "LaboratorID", referencedColumnName = "LaboratorID")
    @ManyToOne(cascade = CascadeType.ALL,optional = false)
    private LaboratorSystem laboratorID;

    public ExctractionInformation() {
    }

    public ExctractionInformation(Integer preparationID) {
        this.preparationID = preparationID;
    }

    public ExctractionInformation(Integer preparationID, String labCaseNumber, String extractionMethod, Date date, String exctractionInfo) {
        this.preparationID = preparationID;
        this.labCaseNumber = labCaseNumber;
        this.extractionMethod = extractionMethod;
        this.date = date;
        this.exctractionInfo = exctractionInfo;
    }

    public Integer getPreparationID() {
        return preparationID;
    }

    public void setPreparationID(Integer preparationID) {
        this.preparationID = preparationID;
    }

    public String getLabCaseNumber() {
        return labCaseNumber;
    }

    public void setLabCaseNumber(String labCaseNumber) {
        this.labCaseNumber = labCaseNumber;
    }

    public String getExtractionMethod() {
        return extractionMethod;
    }

    public void setExtractionMethod(String extractionMethod) {
        this.extractionMethod = extractionMethod;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getExctractionInfo() {
        return exctractionInfo;
    }

    public void setExctractionInfo(String exctractionInfo) {
        this.exctractionInfo = exctractionInfo;
    }

    public LaboratorSystem getLaboratorID() {
        return laboratorID;
    }

    public void setLaboratorID(LaboratorSystem laboratorID) {
        this.laboratorID = laboratorID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (preparationID != null ? preparationID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExctractionInformation)) {
            return false;
        }
        ExctractionInformation other = (ExctractionInformation) object;
        if ((this.preparationID == null && other.preparationID != null) || (this.preparationID != null && !this.preparationID.equals(other.preparationID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fims.BLL.ExctractionInformation[ preparationID=" + preparationID + " ]";
    }
    
}
