/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.BLL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "Suspect_Information")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SuspectInformation.findAll", query = "SELECT s FROM SuspectInformation s")
    , @NamedQuery(name = "SuspectInformation.findBySuspectID", query = "SELECT s FROM SuspectInformation s WHERE s.suspectID = :suspectID")
    , @NamedQuery(name = "SuspectInformation.findByFullName", query = "SELECT s FROM SuspectInformation s WHERE s.fullName = :fullName")
    , @NamedQuery(name = "SuspectInformation.findByGender", query = "SELECT s FROM SuspectInformation s WHERE s.gender = :gender")
    , @NamedQuery(name = "SuspectInformation.findByDateOfBirth", query = "SELECT s FROM SuspectInformation s WHERE s.dateOfBirth = :dateOfBirth")
    , @NamedQuery(name = "SuspectInformation.findByCriminalHistory", query = "SELECT s FROM SuspectInformation s WHERE s.criminalHistory = :criminalHistory")
    , @NamedQuery(name = "SuspectInformation.findByHeight", query = "SELECT s FROM SuspectInformation s WHERE s.height = :height")
    , @NamedQuery(name = "SuspectInformation.findByWeight", query = "SELECT s FROM SuspectInformation s WHERE s.weight = :weight")
    , @NamedQuery(name = "SuspectInformation.findByAddress", query = "SELECT s FROM SuspectInformation s WHERE s.address = :address")
    , @NamedQuery(name = "SuspectInformation.findByCity", query = "SELECT s FROM SuspectInformation s WHERE s.city = :city")
    , @NamedQuery(name = "SuspectInformation.findByState", query = "SELECT s FROM SuspectInformation s WHERE s.state = :state")
    , @NamedQuery(name = "SuspectInformation.findByZipCode", query = "SELECT s FROM SuspectInformation s WHERE s.zipCode = :zipCode")})
public class SuspectInformation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "SuspectID")
    @GeneratedValue(generator="InvSeq")
    @SequenceGenerator(name="InvSeq",sequenceName="INV_SEQ", allocationSize=1)
    private Integer suspectID;
    @Basic(optional = false)
    @Column(name = "Full_Name")
    private String fullName;
    @Basic(optional = false)
    @Column(name = "Gender")
    private String gender;
    @Basic(optional = false)
    @Column(name = "Date_Of_Birth")
    private String dateOfBirth;
    @Basic(optional = false)
    @Column(name = "Criminal_History")
    private String criminalHistory;
    @Basic(optional = false)
    @Column(name = "Height")
    private String height;
    @Basic(optional = false)
    @Column(name = "Weight")
    private String weight;
    @Basic(optional = false)
    @Column(name = "Address")
    private String address;
    @Basic(optional = false)
    @Column(name = "City")
    private String city;
    @Basic(optional = false)
    @Column(name = "State")
    private String state;
    @Basic(optional = false)
    @Column(name = "Zip_Code")
    private String zipCode;

    public SuspectInformation() {
    }

    public SuspectInformation(Integer suspectID) {
        this.suspectID = suspectID;
    }

    public SuspectInformation(Integer suspectID, String fullName, String gender, String dateOfBirth, String criminalHistory, String height, String weight, String address, String city, String state, String zipCode) {
        this.suspectID = suspectID;
        this.fullName = fullName;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.criminalHistory = criminalHistory;
        this.height = height;
        this.weight = weight;
        this.address = address;
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
    }

    public Integer getSuspectID() {
        return suspectID;
    }

    public void setSuspectID(Integer suspectID) {
        this.suspectID = suspectID;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getCriminalHistory() {
        return criminalHistory;
    }

    public void setCriminalHistory(String criminalHistory) {
        this.criminalHistory = criminalHistory;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (suspectID != null ? suspectID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SuspectInformation)) {
            return false;
        }
        SuspectInformation other = (SuspectInformation) object;
        if ((this.suspectID == null && other.suspectID != null) || (this.suspectID != null && !this.suspectID.equals(other.suspectID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fims.BLL.SuspectInformation[ suspectID=" + suspectID + " ]";
    }
    
}
