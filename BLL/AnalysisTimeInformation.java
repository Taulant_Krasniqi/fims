/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.BLL;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "Analysis_Time_Information")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AnalysisTimeInformation.findAll", query = "SELECT a FROM AnalysisTimeInformation a")
    , @NamedQuery(name = "AnalysisTimeInformation.findByEvidenceID", query = "SELECT a FROM AnalysisTimeInformation a WHERE a.evidenceID = :evidenceID")
    , @NamedQuery(name = "AnalysisTimeInformation.findByLabCaseNumber", query = "SELECT a FROM AnalysisTimeInformation a WHERE a.labCaseNumber = :labCaseNumber")
    , @NamedQuery(name = "AnalysisTimeInformation.findByWorkDateBegins", query = "SELECT a FROM AnalysisTimeInformation a WHERE a.workDateBegins = :workDateBegins")
    , @NamedQuery(name = "AnalysisTimeInformation.findByWorkDateEnds", query = "SELECT a FROM AnalysisTimeInformation a WHERE a.workDateEnds = :workDateEnds")
    , @NamedQuery(name = "AnalysisTimeInformation.findByAnalyst", query = "SELECT a FROM AnalysisTimeInformation a WHERE a.analyst = :analyst")})
public class AnalysisTimeInformation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "EvidenceID")
    @GeneratedValue(generator="InvSeq")
    @SequenceGenerator(name="InvSeq",sequenceName="INV_SEQ", allocationSize=1)
    private Integer evidenceID;
    @Basic(optional = false)
    @Column(name = "Lab_Case_Number")
    private String labCaseNumber;
    @Basic(optional = false)
    @Column(name = "Work_Date_Begins")
    @Temporal(TemporalType.DATE)
    private Date workDateBegins;
    @Basic(optional = false)
    @Column(name = "Work_Date_Ends")
    @Temporal(TemporalType.DATE)
    private Date workDateEnds;
    @Basic(optional = false)
    @Column(name = "Analyst")
    private String analyst;
    @JoinColumn(name = "LaboratorID", referencedColumnName = "LaboratorID")
    @ManyToOne(cascade = CascadeType.ALL,optional = false)
    private LaboratorSystem laboratorID;

    public AnalysisTimeInformation() {
    }

    public AnalysisTimeInformation(Integer evidenceID) {
        this.evidenceID = evidenceID;
    }

    public AnalysisTimeInformation(Integer evidenceID, String labCaseNumber, Date workDateBegins, Date workDateEnds, String analyst) {
        this.evidenceID = evidenceID;
        this.labCaseNumber = labCaseNumber;
        this.workDateBegins = workDateBegins;
        this.workDateEnds = workDateEnds;
        this.analyst = analyst;
    }

    public Integer getEvidenceID() {
        return evidenceID;
    }

    public void setEvidenceID(Integer evidenceID) {
        this.evidenceID = evidenceID;
    }

    public String getLabCaseNumber() {
        return labCaseNumber;
    }

    public void setLabCaseNumber(String labCaseNumber) {
        this.labCaseNumber = labCaseNumber;
    }

    public Date getWorkDateBegins() {
        return workDateBegins;
    }

    public void setWorkDateBegins(Date workDateBegins) {
        this.workDateBegins = workDateBegins;
    }

    public Date getWorkDateEnds() {
        return workDateEnds;
    }

    public void setWorkDateEnds(Date workDateEnds) {
        this.workDateEnds = workDateEnds;
    }

    public String getAnalyst() {
        return analyst;
    }

    public void setAnalyst(String analyst) {
        this.analyst = analyst;
    }

    public LaboratorSystem getLaboratorID() {
        return laboratorID;
    }

    public void setLaboratorID(LaboratorSystem laboratorID) {
        this.laboratorID = laboratorID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (evidenceID != null ? evidenceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnalysisTimeInformation)) {
            return false;
        }
        AnalysisTimeInformation other = (AnalysisTimeInformation) object;
        if ((this.evidenceID == null && other.evidenceID != null) || (this.evidenceID != null && !this.evidenceID.equals(other.evidenceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fims.BLL.AnalysisTimeInformation[ evidenceID=" + evidenceID + " ]";
    }
    
}
