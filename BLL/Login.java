/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "Login")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Login.findAll", query = "SELECT l FROM Login l")
    , @NamedQuery(name = "Login.findByLoginID", query = "SELECT l FROM Login l WHERE l.loginID = :loginID")
    , @NamedQuery(name = "Login.findByNickName", query = "SELECT l FROM Login l WHERE l.nickName = :nickName")
    , @NamedQuery(name = "Login.findByPassword", query = "SELECT l FROM Login l WHERE l.password = :password")})
public class Login implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "loginID")
    private Collection<Regjistrimi> regjistrimiCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "LoginID")
    @GeneratedValue(generator="InvSeq")
    @SequenceGenerator(name="InvSeq",sequenceName="INV_SEQ", allocationSize=1)
    private Integer loginID;
    @Basic(optional = false)
    @Column(name = "NickName")
    private String nickName;
    @Basic(optional = false)
    @Column(name = "Password")
    private String password;

    public Login() {
    }

    public Login(Integer loginID) {
        this.loginID = loginID;
    }

    public Login(Integer loginID, String nickName, String password) {
        this.loginID = loginID;
        this.nickName = nickName;
        this.password = password;
    }

    public Integer getLoginID() {
        return loginID;
    }

    public void setLoginID(Integer loginID) {
        this.loginID = loginID;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (loginID != null ? loginID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Login)) {
            return false;
        }
        Login other = (Login) object;
        if ((this.loginID == null && other.loginID != null) || (this.loginID != null && !this.loginID.equals(other.loginID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fims.BLL.Login[ loginID=" + loginID + " ]";
    }

    @XmlTransient
    public Collection<Regjistrimi> getRegjistrimiCollection() {
        return regjistrimiCollection;
    }

    public void setRegjistrimiCollection(Collection<Regjistrimi> regjistrimiCollection) {
        this.regjistrimiCollection = regjistrimiCollection;
    }
    
}
