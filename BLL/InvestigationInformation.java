/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fims.BLL;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "Investigation_Information")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InvestigationInformation.findAll", query = "SELECT i FROM InvestigationInformation i")
    , @NamedQuery(name = "InvestigationInformation.findByInvestigationID", query = "SELECT i FROM InvestigationInformation i WHERE i.investigationID = :investigationID")
    , @NamedQuery(name = "InvestigationInformation.findByLabCaseNumber", query = "SELECT i FROM InvestigationInformation i WHERE i.labCaseNumber = :labCaseNumber")
    , @NamedQuery(name = "InvestigationInformation.findByInvestigationOfficer", query = "SELECT i FROM InvestigationInformation i WHERE i.investigationOfficer = :investigationOfficer")
    , @NamedQuery(name = "InvestigationInformation.findByRank", query = "SELECT i FROM InvestigationInformation i WHERE i.rank = :rank")
    , @NamedQuery(name = "InvestigationInformation.findBySubjectOfInvestigation", query = "SELECT i FROM InvestigationInformation i WHERE i.subjectOfInvestigation = :subjectOfInvestigation")
    , @NamedQuery(name = "InvestigationInformation.findByDateOfCrime", query = "SELECT i FROM InvestigationInformation i WHERE i.dateOfCrime = :dateOfCrime")
    , @NamedQuery(name = "InvestigationInformation.findByAddressOfCrime", query = "SELECT i FROM InvestigationInformation i WHERE i.addressOfCrime = :addressOfCrime")
    , @NamedQuery(name = "InvestigationInformation.findByCity", query = "SELECT i FROM InvestigationInformation i WHERE i.city = :city")})
public class InvestigationInformation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "InvestigationID")
    @GeneratedValue(generator="InvSeq")
    @SequenceGenerator(name="InvSeq",sequenceName="INV_SEQ", allocationSize=1)
    private Integer investigationID;
    @Basic(optional = false)
    @Column(name = "Lab_Case_Number")
    private String labCaseNumber;
    @Basic(optional = false)
    @Column(name = "Investigation_Officer")
    private String investigationOfficer;
    @Basic(optional = false)
    @Column(name = "Rank")
    private String rank;
    @Basic(optional = false)
    @Column(name = "Subject_Of_Investigation")
    private String subjectOfInvestigation;
    @Basic(optional = false)
    @Column(name = "Date_Of_Crime")
    @Temporal(TemporalType.DATE)
    private Date dateOfCrime;
    @Basic(optional = false)
    @Column(name = "Address_Of_Crime")
    private String addressOfCrime;
    @Basic(optional = false)
    @Column(name = "City")
    private String city;
    @JoinColumn(name = "LaboratorID", referencedColumnName = "LaboratorID")
    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    private LaboratorSystem laboratorID;

    public InvestigationInformation() {
    }

    public InvestigationInformation(Integer investigationID) {
        this.investigationID = investigationID;
    }

    public InvestigationInformation(Integer investigationID, String labCaseNumber, String investigationOfficer, String rank, String subjectOfInvestigation, Date dateOfCrime, String addressOfCrime, String city) {
        this.investigationID = investigationID;
        this.labCaseNumber = labCaseNumber;
        this.investigationOfficer = investigationOfficer;
        this.rank = rank;
        this.subjectOfInvestigation = subjectOfInvestigation;
        this.dateOfCrime = dateOfCrime;
        this.addressOfCrime = addressOfCrime;
        this.city = city;
    }

    public Integer getInvestigationID() {
        return investigationID;
    }

    public void setInvestigationID(Integer investigationID) {
        this.investigationID = investigationID;
    }

    public String getLabCaseNumber() {
        return labCaseNumber;
    }

    public void setLabCaseNumber(String labCaseNumber) {
        this.labCaseNumber = labCaseNumber;
    }

    public String getInvestigationOfficer() {
        return investigationOfficer;
    }

    public void setInvestigationOfficer(String investigationOfficer) {
        this.investigationOfficer = investigationOfficer;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getSubjectOfInvestigation() {
        return subjectOfInvestigation;
    }

    public void setSubjectOfInvestigation(String subjectOfInvestigation) {
        this.subjectOfInvestigation = subjectOfInvestigation;
    }

    public Date getDateOfCrime() {
        return dateOfCrime;
    }

    public void setDateOfCrime(Date dateOfCrime) {
        this.dateOfCrime = dateOfCrime;
    }

    public String getAddressOfCrime() {
        return addressOfCrime;
    }

    public void setAddressOfCrime(String addressOfCrime) {
        this.addressOfCrime = addressOfCrime;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public LaboratorSystem getLaboratorID() {
        return laboratorID;
    }

    public void setLaboratorID(LaboratorSystem laboratorID) {
        this.laboratorID = laboratorID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (investigationID != null ? investigationID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InvestigationInformation)) {
            return false;
        }
        InvestigationInformation other = (InvestigationInformation) object;
        if ((this.investigationID == null && other.investigationID != null) || (this.investigationID != null && !this.investigationID.equals(other.investigationID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fims.BLL.InvestigationInformation[ investigationID=" + investigationID + " ]";
    }
    
}
